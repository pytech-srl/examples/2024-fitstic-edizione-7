from django.shortcuts import render


def hello_world(request):

    return render(
        request,
        'hello.html',
        {"testo_personalizzato": "Ciao a tutti!"}
    )


def seconda_pagina(request):

    return render(
        request,
        'seconda_pagina.html',
        {"altro_testo": "Altro!"}
    )
