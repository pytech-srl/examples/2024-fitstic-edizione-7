from django.urls import path

from polls.views import QuestionListView, QuestionDetailView, QuestionCreateView

urlpatterns = [
    path(
        "question/list/",
        QuestionListView.as_view(),
        name="question_list"
    ),
    path(
        "question/new/",
        QuestionCreateView.as_view(),
        name="question_create"
    ),
    path(
        "question/<int:pk>/",
        QuestionDetailView.as_view(),
        name="question_detail"
    ),
]

