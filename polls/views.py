from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DetailView, ListView

from polls.models import Question

__all__ = [
    "QuestionCreateView",
    "QuestionDetailView",
    "QuestionListView",
]


class QuestionCreateView(CreateView):
    model = Question
    fields = [
        "text",
        "pub_date",
    ]
#    success_url = reverse_lazy("question_list")

    def get_success_url(self):
        return reverse_lazy(
            "question_detail",
            kwargs={"pk": self.object.pk}
        )


class QuestionDetailView(DetailView):
    model = Question


class QuestionListView(ListView):
    model = Question
